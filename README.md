# Netdata plugin for DBLab Engine

[Netdata](https://github.com/netdata/netdata) is high-fidelity infrastructure monitoring and troubleshooting. Here you can find a plugin for Netdata that monitors the state of a [DBLab Engine](https://github.com/postgres-ai/database-lab-engine) instance.

---

## Requirements

To monitor the Sync instance, DBLab Engine 3.2.0 or higher is required.

## Installation

### Option 1: Run in a Docker continer (recommended)
```bash
sudo docker run -d \
  --name=netdata \
  --network host \
  --hostname=$(hostname) \
  -p 19999:19999 \
  -v netdataconfig:/etc/netdata \
  -v netdatalib:/var/lib/netdata \
  -v netdatacache:/var/cache/netdata \
  -v /proc:/host/proc:ro \
  -v /sys:/host/sys:ro \
  -v /etc/os-release:/host/etc/os-release:ro \
  --restart unless-stopped \
  --cap-add SYS_PTRACE \
  --security-opt apparmor=unconfined \
  postgresai/netdata-for-dle:v1.47.5-1
```
Now, Netdata dashboard should be accessible at `http://<server_ip>:19999`.

In the command above, adjust network-related options (`-p`, `--network`) according to your security-related requirements (but note, that the container with Netdata must be able to communicate with DBLab Engine API, so check how exactly the DBLab Engine's main container was launched).

### Option 2: `install.sh`
The following one-liner installs Netdata with the DBLab Engine plugin into a Linux system:
```bash
bash <(curl -Ss https://gitlab.com/postgres-ai/netdata_for_dle/-/raw/main/install.sh)
```

Now, Netdata dashboard should be accessible at `http://<server_ip>:19999`.

## Configuration
### How to make configuration changes
The following commands show how configuration inside the container with Netdata can be adjusted. Note that to make your changes persistent, you need to find a way to save the changes (for example, prepare an adjusted Docker image or use `-v` to keep the configuration files outside):
```bash
sudo docker exec -it netdata bash
cd /etc/netdata
./edit-config netdata.conf  # Netdata main configuration file

./edit-config charts.d/DBLab_Engine.conf  # Plugin configuration for DBLab Engine
./edit-config health.d/DBLab_Engine.conf  # Health configuration for DBLab Engine plugin

exit

# Restart to apply the changes
sudo docker restart netdata
```

### What to change first
#### "DBLab Engine" plugin
In most cases, it is enough to change only the value of `DBLab_Engine_token`. Example:
```
DBLab_Engine_token=secret_token
DBLab_Engine_protocol=http
DBLab_Engine_ip=127.0.0.1
DBLab_Engine_port=2345
```

After making a change, do not forget to restart the agent as described above.

### Notifications

Netdata offers two ways to receive alarm notifications on external platforms:

- The **Netdata Cloud** offers [centralized alarm notifications](https://learn.netdata.cloud/docs/monitor/enable-notifications#netdata-cloud) via email, which leverages the health status information already streamed to Netdata Cloud from connected nodes to send notifications to those who have enabled them.

- The **Netdata Agent** has a [notification system](https://learn.netdata.cloud/docs/monitor/enable-notifications#netdata-agent) that supports more than a dozen services, such as email, Slack, PagerDuty, Twilio, Amazon SNS, Discord, and much [more](https://learn.netdata.cloud/docs/monitor/enable-notifications).

Read the doc on [configuring alarms](https://learn.netdata.cloud/docs/monitor/configure-alarms) to change the preconfigured thresholds or to create tailored alarms for your infrastructure.

### Useful links
- [Netdata in Docker](https://learn.netdata.cloud/docs/agent/packaging/docker)
- [Netdata agent configuration](https://learn.netdata.cloud/docs/configure/nodes)
- [The basics of configuring Netdata](https://learn.netdata.cloud/guides/step-by-step/step-04)
- [Configure health alarms](https://learn.netdata.cloud/docs/monitor/configure-alarms)
- [Enable alarm notifications](https://learn.netdata.cloud/docs/monitor/enable-notifications)
---

## Netdata plugin for DBLab Engine – monitoring reference
The plugin to monitor the state of DBLab Engine in Netdata.

### "DBLab Engine" pluging charts
- **DBLab Engine Status**
    - status
- **DBLab Engine Uptime**
    - uptime
- **DBLab Engine Retrieval Status**
    - status
- **Number of snapshots**
    - count
- **Newest data state**
    - lag
- **Oldest data state**
    - lag
- **Number of Clones**
    - total_clones
    - older_than_1_day
    - older_than_7_days
    - older_than_30_days
- **Average Cloning Time**
    - time
- **Sync instance uptime**
    - uptime
- **Sync instance lag**
    - lag time

### "DBLab Engine" plugin alarms
- **status**
    - critical - if the DBLab Engine is unhealthy (`status.code <> 'OK'`)
- **uptime**
    - critical - if the DBLab Engine is not available
- **data_state**
    - warning - if there is a newest snapshot data state of more than 1 day
    - critical - if there is a newest snapshot data state of more than 7 days
    - warning - if there is a oldest snapshot data state of more than 30 days
- **num_clones**
    - warning - if clones older than 7 days are detected
    - critical - if clones older than 30 days are detected
- **sync_instance_uptime**
    - warning - if DBLab Engine Sync instance uptime below 10 minutes
- **sync_instance_lag**
    - warning - if there is a lag of more than 1 hour
    - critical - if there is a lag of more than 1 day

You can tune the alarm triggers by changing the values in the configuration file `/etc/netdata/health.d/DBLab_Engine.conf`

## Troubleshooting and debugging

If you don't see "`DBLab Engine`" in the Netdata side menu, you can run this command to debugging the DBLab Engine plugin:

```bash
sudo /usr/libexec/netdata/plugins.d/charts.d.plugin debug DBLab_Engine
# for docker
sudo docker exec -it netdata /usr/libexec/netdata/plugins.d/charts.d.plugin debug DBLab_Engine
```

Example:

```
ERROR: DBLab_Engine: DBLab Engine is not available
```

The DBLab_Engine is not available at the time of launch netdata. Or the connection (or `DBLab_Engine_token`) parameters may have been specified incorrectly.
