# netdata charts.d plugin for DBLab Engine
# /usr/libexec/netdata/charts.d/DBLab_Engine.chart.sh

# _update_every is a special variable - it holds the number of seconds
# between the calls of the _update() function
DBLab_Engine_update_every=10

# the priority is used to sort the charts on the dashboard
# 1 = the first chart
DBLab_Engine_priority=4000

# global variables
DBLab_Engine_token=secret_token
DBLab_Engine_protocol=http
DBLab_Engine_ip=127.0.0.1
DBLab_Engine_port=2345

# All variables can be overridden in /etc/netdata/charts.d/DBLab_Engine.conf

DBLab_Engine_api_get() {
  # get data from the DBLab Engine API
  DBLab_Engine_api_status=$(curl -s -H "Verification-Token: $DBLab_Engine_token" "$DBLab_Engine_protocol"://"$DBLab_Engine_ip":"$DBLab_Engine_port"/status)
  DBLab_Engine_api_snapshots=$(curl -s -H "Verification-Token: $DBLab_Engine_token" "$DBLab_Engine_protocol"://"$DBLab_Engine_ip":"$DBLab_Engine_port"/snapshots)

  return 0
}

DBLab_Engine_get() {
  # do all the work to collect
  DBLab_Engine_api_get || return 1

  DBLab_Engine_status=$(
    if [[ "$(echo "$DBLab_Engine_api_status" | jq -r '.status.code')" = "OK" ]]
      then echo 1
      else echo 0
    fi
  )

  DBLab_Engine_uptime=$(
    engine_startedAt=$(echo "$DBLab_Engine_api_status" | jq -r '.engine.startedAt')
    if [[ -z "$engine_startedAt" ]]
      then echo 0
      else echo "$(( $(TZ="UTC" date +%s) - $(TZ="UTC" date -d "$engine_startedAt" +%s) ))"
    fi
  )

  DBLab_Engine_retrieving_status=$(
    retrieving_status=$(echo "$DBLab_Engine_api_status" | jq -r '.retrieving.status')
      if [[ "$retrieving_status" = "finished" ]]
        then echo 1
        elif [[ "$retrieving_status" = "refreshing" ]]
          then echo 2
        else echo 0
      fi
  )

  DBLab_Engine_snapshots_count=$(
    snapshots_count=$(echo "$DBLab_Engine_api_snapshots" | jq -r '. | length')
    if [[ -z "$snapshots_count" ]]
      then echo 0
      else echo "$snapshots_count"
    fi
  )

  DBLab_Engine_newest_data_state=$(
    dataStateAt=$(echo "$DBLab_Engine_api_snapshots" | jq -r '. | sort_by(.dataStateAt) | max | .dataStateAt')
    if [[ -z "$dataStateAt" ]] || [[ "$dataStateAt" = "null" ]]
      then echo 0
      else echo "$(( $(TZ="UTC" date +%s) - $(TZ="UTC" date -d "$dataStateAt" +%s) ))"
    fi
  )

  DBLab_Engine_oldest_data_state=$(
    dataStateAt=$(echo "$DBLab_Engine_api_snapshots" | jq -r '. | sort_by(.dataStateAt) | min | .dataStateAt')
    if [[ -z "$dataStateAt" ]] || [[ "$dataStateAt" = "null" ]]
      then echo 0
      else echo "$(( $(TZ="UTC" date +%s) - $(TZ="UTC" date -d "$dataStateAt" +%s) ))"
    fi
  )

  DBLab_Engine_average_cloning_time=$(
    echo "$DBLab_Engine_api_status" | jq -r '.cloning.expectedCloningTime | floor'
  )

  DBLab_Engine_num_clones=$(
    echo "$DBLab_Engine_api_status" | jq -r '.cloning.numClones'
  )

  DBLab_Engine_num_clones_older_than_1_day=$(
    echo $DBLab_Engine_api_status | jq -r '.cloning.clones[] | select(.snapshot.dataStateAt < ( now - 86400 | strftime("%Y-%m-%dT%H:%M:%SZ"))) | .id' | wc -l
  )

  DBLab_Engine_num_clones_older_than_7_days=$(
    echo $DBLab_Engine_api_status | jq -r '.cloning.clones[] | select(.snapshot.dataStateAt < ( now - 604800 | strftime("%Y-%m-%dT%H:%M:%SZ"))) | .id' | wc -l
  )

  DBLab_Engine_num_clones_older_than_30_days=$(
    echo $DBLab_Engine_api_status | jq -r '.cloning.clones[] | select(.snapshot.dataStateAt < ( now - 2592000 | strftime("%Y-%m-%dT%H:%M:%SZ"))) | .id' | wc -l
  )

  DBLab_Engine_sync_instance_uptime=$(
    replicationUptime=$(echo "$DBLab_Engine_api_status" | jq -r '.synchronization.replicationUptime')
    if [[ -z "$replicationUptime" ]]
      then echo 0
      else echo "$replicationUptime"
    fi
  )

  DBLab_Engine_sync_instance_lag=$(
    replicationLag=$(echo "$DBLab_Engine_api_status" | jq -r '.synchronization.replicationLag')
    if [[ -z "$replicationLag" ]]
      then echo 0
      else echo "$replicationLag"
    fi
  )

  return 0
}

# _check is called once, to find out if this chart should be enabled or not
DBLab_Engine_check() {
  # check if the required command exists
  require_cmd jq || return 1

  # check that DBLab_Engine is available
  if [[ "$(curl -s -H "Verification-Token: $DBLab_Engine_token" "$DBLab_Engine_protocol"://"$DBLab_Engine_ip":"$DBLab_Engine_port"/status | jq -r '.status.code')" != "OK" ]]; then
    error "DBLab Engine is not available"
    return 1
  fi

  return 0
}

# _create is called once, to create the charts
DBLab_Engine_create() {
  # create the charts
  cat << EOF
CHART DBLab_Engine.status '' "DBLab Engine Status" "status" 'status' '' line $((DBLab_Engine_priority +1 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION status '' absolute 1 1
CHART DBLab_Engine.uptime '' "DBLab Engine Uptime" "seconds" 'uptime' '' area $((DBLab_Engine_priority +2 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION uptime '' absolute 1 1
CHART DBLab_Engine.retrieving_status '' "DBLab Engine Retrieval Status" "status" 'retrieving' '' line $((DBLab_Engine_priority +3 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION status '' absolute 1 1
CHART DBLab_Engine.snapshots_count '' "Number of snapshots" "count" 'snapshots' '' line $((DBLab_Engine_priority +4 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION count '' absolute 1 1
CHART DBLab_Engine.newest_data_state '' "Newest data state (lag)" "seconds" 'newest_data_state' '' area $((DBLab_Engine_priority +5 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION lag '' absolute 1 1
CHART DBLab_Engine.oldest_data_state '' "Oldest data state (lag)" "seconds" 'oldest_data_state' '' area $((DBLab_Engine_priority +6 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION lag '' absolute 1 1
CHART DBLab_Engine.num_clones '' "Number of Clones" "count" 'clones' '' line $((DBLab_Engine_priority +7 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION total_clones '' absolute 1 1
DIMENSION older_than_1_day '' absolute 1 1
DIMENSION older_than_7_days '' absolute 1 1
DIMENSION older_than_30_days '' absolute 1 1
CHART DBLab_Engine.average_cloning_time '' "Average Cloning Time" "seconds" 'cloning time' '' area $((DBLab_Engine_priority +8 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION avg '' absolute 1 1
CHART DBLab_Engine.sync_instance_uptime '' "Sync Instance Uptime" "seconds" 'sync_instance_uptime' '' area $((DBLab_Engine_priority +9 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION sync_uptime '' absolute 1 1
CHART DBLab_Engine.sync_instance_lag '' "Sync Instance Lag" "seconds" 'sync_instance_lag' '' area $((DBLab_Engine_priority +10 )) $DBLab_Engine_update_every '' '' 'DBLab_Engine'
DIMENSION sync_lag '' absolute 1 1
EOF

  return 0
}

# _update is called continuously, to collect the values
DBLab_Engine_update() {
  # the first argument to this function is the microseconds since last update
  # pass this parameter to the BEGIN statement (see below).

  DBLab_Engine_get || return 1

  # write the result of the work.
  cat << VALUESEOF
BEGIN DBLab_Engine.status $1
SET status = $DBLab_Engine_status
END
BEGIN DBLab_Engine.uptime $1
SET uptime = $DBLab_Engine_uptime
END
BEGIN DBLab_Engine.retrieving_status $1
SET status = $DBLab_Engine_retrieving_status
END
BEGIN DBLab_Engine.snapshots_count $1
SET count = $DBLab_Engine_snapshots_count
END
BEGIN DBLab_Engine.newest_data_state $1
SET lag = $DBLab_Engine_newest_data_state
END
BEGIN DBLab_Engine.oldest_data_state $1
SET lag = $DBLab_Engine_oldest_data_state
END
BEGIN DBLab_Engine.num_clones $1
SET total_clones = $DBLab_Engine_num_clones
SET older_than_1_day = $DBLab_Engine_num_clones_older_than_1_day
SET older_than_7_days = $DBLab_Engine_num_clones_older_than_7_days
SET older_than_30_days = $DBLab_Engine_num_clones_older_than_30_days
END
BEGIN DBLab_Engine.average_cloning_time $1
SET avg = $DBLab_Engine_average_cloning_time
END
BEGIN DBLab_Engine.sync_instance_uptime $1
SET sync_uptime = $DBLab_Engine_sync_instance_uptime
END
BEGIN DBLab_Engine.sync_instance_lag $1
SET sync_lag = $DBLab_Engine_sync_instance_lag
END
VALUESEOF

  return 0
}
