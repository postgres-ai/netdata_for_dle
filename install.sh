#!/bin/bash
set -x

# instal netdata
echo -e "\e[42mInstal netdata\e[0m"
bash <(curl -Ss https://my-netdata.io/kickstart.sh) --disable-telemetry --stable-channel --dont-wait

# install dependencies
if ! command -v jq &> /dev/null; then
  echo -e "\e[42mInstall dependencies\e[0m"
  # Check which package managers are available
  APT_GET_CMD=$(which apt-get)
  if [[ -n $APT_GET_CMD ]]; then
    sudo apt-get install -y jq
  else
    echo -e "\e[91mYou have to install jq packages\e[0m"
    exit 1
  fi
fi

# copy DBLab Engine plugin
echo -e "\e[42mCopy DBLab Engine plugin\e[0m"
sudo curl https://gitlab.com/postgres-ai/netdata_for_dle/-/raw/main/dblab_plugin/DBLab_Engine.chart.sh \
  --output /usr/libexec/netdata/charts.d/DBLab_Engine.chart.sh

# copy DBLab Engine plugin conf
echo -e "\e[42mCopy DBLab Engine plugin conf\e[0m"
sudo curl https://gitlab.com/postgres-ai/netdata_for_dle/-/raw/main/dblab_plugin/DBLab_Engine.conf \
  --output /etc/netdata/charts.d/DBLab_Engine.conf

# copy DBLab Engine health conf
echo -e "\e[42mCopy DBLab Engine health conf\e[0m"
sudo curl https://gitlab.com/postgres-ai/netdata_for_dle/-/raw/main/dblab_plugin/health.d/DBLab_Engine.conf \
  --output /etc/netdata/health.d/DBLab_Engine.conf

# netdata.conf
echo -e "\e[42mConfigure netdata.conf\e[0m"
sudo curl http://127.0.0.1:19999/netdata.conf \
  --output /etc/netdata/netdata.conf
## update every = 10
# sudo sed -ri "s/^(\s*)(# update every = 1$)/\1update every = 10/" /etc/netdata/netdata.conf

# restart netdata
echo -e "\e[42mRestart netdata\e[0m"
sudo systemctl restart netdata
