ARG NETDATA_VERSION=v1.47.5

FROM netdata/netdata:${NETDATA_VERSION}
LABEL maintainer="postgres.ai"

# Disable anonymous statistics
ENV DO_NOT_TRACK=1

# Install dependencies
RUN apt-get update && apt-get install -y jq coreutils

# Modify /usr/sbin/run.sh to ignore errors during the copying step
RUN sed -i '/cp -an \/etc\/netdata\.stock\/\*/s/$/ || true/' /usr/sbin/run.sh \
    && sed -i '/cp -an \/etc\/netdata\.stock\/\.\[\^\.\]\*/s/$/ || true/' /usr/sbin/run.sh

# Copy DBLab Engine plugin
COPY dblab_plugin/DBLab_Engine.chart.sh /usr/libexec/netdata/charts.d/DBLab_Engine.chart.sh

# Copy DBLab Engine plugin conf
COPY dblab_plugin/DBLab_Engine.conf /etc/netdata/charts.d/DBLab_Engine.conf

# Copy DBLab Engine health conf
COPY dblab_plugin/health.d/DBLab_Engine.conf /etc/netdata/health.d/DBLab_Engine.conf

ENV NETDATA_LISTENER_PORT 19999
EXPOSE $NETDATA_LISTENER_PORT

ENTRYPOINT ["/usr/sbin/run.sh"]

HEALTHCHECK --interval=60s --timeout=10s --retries=3 CMD /usr/sbin/health.sh
